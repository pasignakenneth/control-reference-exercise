﻿using System;

using Xamarin.Forms;

namespace ControlReferencesExercise
{
    public class User
    {
        public string Name
        {
            get; set;
        }
        public string Age
        {
            get; set;
        }
        public string Address
        {
            get; set;
        }
        public string ImageUrl
        {
            get; set;
        }
        public string Bio
        {
            get; set;
        }

    }
}

