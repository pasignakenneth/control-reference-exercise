﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ControlReferencesExercise
{
    public partial class CreatePage : ContentPage
    {
        User user = new User();
        public static ObservableCollection<User> newUser = new ObservableCollection<User>();

        public CreatePage()
        {
            InitializeComponent();
        }
        void saveButton()
        {
            newUser.Add(new User() { Name = name.Text, Age = age.Text, Address = address.Text, ImageUrl = imageUrl.Text, Bio = bio.Text });
            DisplayAlert("Saved", "Returning to home", "Okay");
            Navigation.PopToRootAsync();

        }
    }


}
