﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ControlReferencesExercise
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        void viewAll()
        {
            Navigation.PushAsync(new ViewAllPage());
        }
        void createP()
        {
            Navigation.PushAsync(new CreatePage());
        }
    }
}
