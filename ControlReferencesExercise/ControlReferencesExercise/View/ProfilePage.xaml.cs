﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ControlReferencesExercise
{
    
    public partial class ProfilePage : ContentPage
    {
        User user;

        public ProfilePage(User user)
        {
            BindingContext = user;
            InitializeComponent();

            this.user = user;
        }
        void EditButton_Clicked(object sender, System.EventArgs e)
        {
            
        }
        void SaveButton_Clicked(object sender, System.EventArgs e)
        {
            
        }
    }
}
