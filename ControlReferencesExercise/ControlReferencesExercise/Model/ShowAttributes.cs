﻿using System;

using Xamarin.Forms;

namespace ControlReferencesExercise.Model
{
    public class ShowAttributes : ContentPage
    {
        public ShowAttributes()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

