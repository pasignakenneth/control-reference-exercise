﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ControlReferencesExercise
{
    public partial class ViewAllPage : ContentPage
    {
        public static User profileInfo;
        public ViewAllPage()
        {
            InitializeComponent();
            userList.ItemsSource = CreatePage.newUser;
        }
        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            var index = CreatePage.newUser.IndexOf((User)userList.SelectedItem);
            profileInfo = new User
            {
                Name = CreatePage.newUser[index].Name,
                Age = CreatePage.newUser[index].Age,
                Address = CreatePage.newUser[index].Address,
                Bio = CreatePage.newUser[index].Bio,
                ImageUrl = CreatePage.newUser[index].ImageUrl
            };

            //var profilePage = new ProfilePage();
            //profilePage.BindingContext = profileInfo;
            Navigation.PushAsync(new ProfilePage(profileInfo));

        }
    }


}
